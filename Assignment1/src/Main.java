import Algorithm.Algorithm;
import Utils.Utilities;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Algorithm algorithm = new Algorithm();
        Scanner scanner = new Scanner(System.in);
        int[] array = Utilities.readDataFromFile();
        while (true) {
            System.out.println();
            System.out.println("------Menu-----");
            System.out.println("1: Input list numbers");
            System.out.println("2: Display number ");
            System.out.println("3: Bubble Sort");
            System.out.println("4: Selection Sort");
            System.out.println("5: Insert Sort");
            System.out.println("6: Search");
            System.out.println("7: Binary Search");
            System.out.println("0: Exit program");
            System.out.println("-------------------------");
            System.out.print("Select option: ");
            int input = scanner.nextInt();
            switch (input) {
                case 1:
                    algorithm.createNewArray();
                    break;
                case 2:
                    Utilities.displayData();
                    break;
                case 3:
                    algorithm.bubbleSortAlgorithm(array);
                    break;
                case 4:
                    algorithm.selectionSort(array);
                    break;
                case 5:
                    algorithm.insertionSort(array);
                    break;
                case 6:
                    algorithm.linearSearch(array);
                    break;
                case 7:
                    algorithm.binarySearch(array);
                    break;
                default:
                    System.exit(0);
            }
        }
    }
}