package Utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public final class Utilities {
    static final String PATH = "/Volumes/T5-Samsung/funix/CSD201x_02-A_VN/fn-alg/Assignment1/";

    public static int[] readDataFromFile() {
        File filePath = new File(PATH + "input.txt");
        Scanner sc;
        try {
            sc = new Scanner(filePath);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }

        ArrayList<String> list = new ArrayList<>();
        while (sc.hasNext()) {
            list.add(sc.next());
        }
        sc.close();
        int size = list.size();
        int[] result = new int[size];
        for (int i = 0; i < size; i++) {
            result[i] = Integer.parseInt(list.get(i));
        }
        return result;
    }

    public static void displayData() {
        int[] data = readDataFromFile();
        for (int element : data) {
            System.out.print(element + "  ");
        }
    }

    public static void writeDataToFile(int[] arrNumbers, int numberOfElements, String fileName) {
        try {
            FileWriter fileWriter = new FileWriter(PATH + fileName);
            for (int i = 0; i < numberOfElements; i++) {
                fileWriter.write(arrNumbers[i] + " ");
            }
            fileWriter.close();
        } catch (IOException exception) {
            System.out.println("Exception occurred");
            exception.printStackTrace();
        }
    }

    public static void printOutData(int[] array) {
        for (int element : array) {
            System.out.println(element);
        }
    }

    public static void printOutDataOneLine(int[] array) {
        for (int element : array) {
            System.out.print(element + " ");
        }
    }

    // Bubble sort
    public static int[] sortArray(int[] array) {
        int size = array.length;
        for (int i = 0; i < size - 1; i++) {
            for (int j = 0; j < size - 1; j++) {
                if(array[j] > array[j+1]) {
                    int temp = array[j];
                    array[j] = array[j+1];
                    array[j+1] = temp;
                }
            }
        }
        return array;
    }
}
