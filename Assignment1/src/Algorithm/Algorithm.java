package Algorithm;
import Utils.Utilities;
import java.util.ArrayList;
import java.util.Scanner;

public class Algorithm {
    public static final Scanner scanner =  new Scanner(System.in);

    public void createNewArray() {
        System.out.print("Input number of elements: ");
        int input = scanner.nextInt();
        // create array with input elements
        int[] arrayNumbers = new int[input];

        for (int i = 0; i<input; i++) {
            System.out.print("Input element at index " + (i+1) + ":");
            int inputNumber = scanner.nextInt();
            arrayNumbers[i] = inputNumber;
        }

        // Write file
        Utilities.writeDataToFile(arrayNumbers, input, "input.txt");
    }

    public void bubbleSortAlgorithm(int[] array) {
        int size = array.length;
        for (int i = 0; i < size - 1; i++) {
            for (int j = 0; j < size - 1; j++) {
                if(array[j] > array[j+1]) {
                    int temp = array[j];
                    array[j] = array[j+1];
                    array[j+1] = temp;
                }
                System.out.println("Print out data each step: " + j);
                Utilities.readDataFromFile();
                System.out.println();
            }
        }
        System.out.println("Final result");
        Utilities.printOutData(array);
        Utilities.writeDataToFile(array, size, "OUTPUT1.txt");
    }

    /// Selection sort.
    /// Time complexity is O(n2)
    public void selectionSort(int[] array) {
        // Choose 1 number is min and then compare with the rest of numbers in array
        int size = array.length;
        for (int i = 0; i < size - 1; i++) {
            int min_idx = i;
            for (int j = i + 1; j < size; j++) {
                // To sort in descending order, change > to < in this line.
                // Select the minimum element in each loop.
                if (array[j] < array[min_idx]) {
                    min_idx = j;
                }
            }

            // swap min number at the correct position
            int temp = array[i];
            array[i] = array[min_idx];
            array[min_idx] = temp;

            System.out.println("Print out data each step: " + i);
            Utilities.printOutDataOneLine(array);
            System.out.println();
        }

        System.out.println("Final result");
        Utilities.printOutData(array);
        Utilities.writeDataToFile(array, size, "OUTPUT2.txt");
    }

    /// Insertion sort
    /// Time complexity O(n2)
    public void insertionSort(int[] array) {
        int size = array.length;

        for (int i =  1; i < size; i++) {
            int key = array[i];
            int j = i - 1;

            while (j >= 0 && array[j] > key) {
                array[j+1] = array[j];
                j--;
            }
            array[j+1] = key;

            System.out.println("Print out data each step: " + i);
            Utilities.printOutDataOneLine(array);
            System.out.println();
        }
        System.out.println("Final result");
        Utilities.printOutData(array);
        Utilities.writeDataToFile(array, size, "OUTPUT3.txt");
    }

    /// Linear Search
    /// Time complexity O(n)
    public void linearSearch(int[] array) {
        int length = array.length;
        ArrayList<Integer>  arrayList = new ArrayList<>();
        // Input value from user
        System.out.print("Input value: ");
        int value = scanner.nextInt();
        for (int i = 0; i < length; i++) {
            if (array[i] > value) {
                arrayList.add(i);
            }
        }
        int size = arrayList.size();
        int[] result = new int[size];
        for (int j = 0; j < size; j++) {
            result[j] = arrayList.get(j);
        }
        System.out.println("Final result");
        Utilities.printOutDataOneLine(result);
        Utilities.writeDataToFile(result, size, "OUTPUT4.txt");
    }

    /// Binary search
    /// Time complexity O(logn)

    public void binarySearch(int[] array) {
        if (array.length == 0) {
            System.out.println("Array is empty");
        }

        // Array's value have to be sorted first
        int[] sortedArray = Utilities.sortArray(array);
        System.out.print("Sorted array: ");
        for (int k = 0; k < sortedArray.length; k++) {
            System.out.print(sortedArray[k] + " ");
        }
        System.out.println();
        // Get middle position index of array
        int low = 0;
        int high = array.length - 1;
        int mid;
        System.out.print("Input value: ");
        int value = scanner.nextInt();
        while (low <= high) {
            mid = (low + high) / 2;
            if (array[mid] > value) {
                mid -= 1;
            } else if (array[mid] < value) {
                mid += 1;
            } else {
                System.out.println("Element found at index: " + mid);
                int[] numbers = new int[1];
                numbers[0] = mid;
                Utilities.writeDataToFile(numbers, numbers.length, "OUTPUT5.txt");
                return;
            }
        }
    }
}
