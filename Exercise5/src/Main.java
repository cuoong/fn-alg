import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int x = 8;
        MyStack stack = new MyStack();
        stack.convertToBinary(x);
    }
}

class Node {
    int info;
    Node next;
    Node(int x, Node p) {
        info = x;
        next = p;
    }
}

class MyStack {
    Node head;
    MyStack() {
        head = null;
    }
    boolean isEmpty() {
        return head == null;
    }

    void push(int x) {
        head = new Node(x, head);
    }

    int pop() {
        int x = head.info;
        head = head.next;
        return x;
    }

    void convertToBinary(int x) {
        int base = 2;
        while (x > 0) {
            // Add reminder value into stack
            push(x % base);
            // Divisible number until x = 0
            x = x / 2;
        }
        while (!isEmpty()) {
            System.out.print(pop());
        }
    }
}